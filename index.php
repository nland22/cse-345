<!DOCTYPE html>
<html>
<head>
	
	<link rel="stylesheet" type="text/css" href="resources/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="resources/css/custom.css">
	<link rel="stylesheet" type="text/css" href="resources/css/style.css">
	<title>Welcome to ClassReg</title>
</head>
<body>
	<nav class="navbar navbar-inverse" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="index.php">ClassReg</a>
			</div>

			<div class="collapse-navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="courses.php">Sign in</a></li>
					<li><a href="semesters.php">Semesters</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="jumbotron">
					<h1>Welcome to the Course Registration Page!</h1>
				</div>
			</div>

		</div>
		<div class="row">
			<div class="col-lg-6">
				<div>
					<a href="available_courses.php"><h3>Courses</h3></a>
				</div>
			</div>
			<div class="col-lg-6">
				<div>
				<a href="semesters.php"><h3>Semester</h3></a>
				</div>
			</div>
		</div>
	</div>
	<p>
	</p>
</div>
<?php

include("database.php");
include("utilities.php");

/* postback logic */

if(isset($_GET["action"]))
{
	$valid = true;
	
	if($_GET["action"] != 'delete') {
		
	}
	
	if ($valid)
	{
		if($_GET["action"] == "update")
		{
			$query = "UPDATE dbo.students SET first_name = ? , last_name = ?  WHERE student_id = ?";
			$params = array(htmlentities($_GET["first_name"],ENT_COMPAT,'iso-8859-1'), htmlentities($_GET["last_name"],ENT_COMPAT,'iso-8859-1'), $_GET["student_id"]);
			sqlsrv_query($conn, $query, $params);
		}

		if($_GET["action"] == "create")
		{
			$query = "INSERT INTO dbo.students (student_id, first_name, last_name) VALUES (?, ?, ?)";
			$params = array(htmlentities($_GET["student_id"],ENT_COMPAT,'iso-8859-1'),htmlentities($_GET["first_name"],ENT_COMPAT,'iso-8859-1'), htmlentities($_GET["last_name"],ENT_COMPAT,'iso-8859-1'));
			sqlsrv_query($conn, $query, $params);
		}
	}

	if($_GET["action"] == "delete")
	{
	   $query = "DELETE FROM dbo.students WHERE student_id = ?";
	   $params = array($_GET["student_id"]);
	   sqlsrv_query($conn, $query, $params); 
	}

}

/* SQL select query */

$query = "SELECT student_id, first_name, last_name FROM dbo.students";
$result = sqlsrv_query($conn, $query);

?> 

</head>

<body>
<?php
echo "<table class='tblStyle'>";

//Create the field headers
echo "<tr class='tblHeaderRow'><td class='tblHeaderCell'> First Name </td><td class='tblHeaderCell'> Last Name </td><td class='tblHeaderCell'> Edit </td><td class='tblHeaderCell'> Delete </td><td class='tblHeaderCell'> Student Schedule </td></tr>";

//display the results
while($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_BOTH))
{
  echo "<tr><td class='tblCell'>" . htmlentities($row["first_name"]) . "</td><td class='tblCell'>" . htmlentities($row["last_name"]) . "</td><td class='tblCell'><a href='Edit.php?student_id=" . $row["student_id"] . "'>Edit</a></td><td class='tblCell'><a href='index.php?action=delete&student_id=" . $row["student_id"] . "'>Delete</a></td><td class='tblCell'><a href='schedule.php?student_id=" . $row["student_id"] . "'>Schedule</a></td></tr>";
}

echo "</table style='tblStyle'>";

echo "<form action='create.php'>";
echo "<input type='submit' value='Create New'>";
echo "</form>";

//close the connection
sqlsrv_close($conn);
?> 
</body>
</html>
</body>
</html>