
<?php


// Validate user entered dates
function validate_date( $date ) {
		// Reformat entered date to: 2004-10-13-10-15-07, pattern is Y-m-d-H-i-s
		$date = str_replace(array('\'', '/', '.', ',', ':', ' '), '-', $date);
		
		$array = explode('-', $date);
		
		$day = $array[2];
		$month = $array[1];
		$year = $array[0];
		if (isset($month) &&  isset($day) && isset($year))
			return  (checkdate($month, $day, $year));
		else
			return false;
}

?>