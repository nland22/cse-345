<html>
<head>
<?php
include("database.php");
include("utilities.php");

?>
</head>

<body>
<form action="available_courses.php" method='get'>
<select name="semester">
<option value="Winter 14">Winter 2014</option>
<option value="Fall 15">Fall 2015</option>
<option value="Spring 15">Spring 2015</option>
</select>
<input type="submit" />
</form>


<?php
if(!empty($_GET['semester'])){

$query = "SELECT     dbo.course_sections.section_id, dbo.courses.course_id, dbo.courses.title, dbo.courses.credits, dbo.course_sections.location
FROM         dbo.course_sections INNER JOIN
                      dbo.courses ON dbo.course_sections.course_id = dbo.courses.course_id
WHERE     (dbo.course_sections.term = '".$_GET['semester']."')";
$result1 = sqlsrv_query($conn, $query);
//echo $query;



echo "<b>Available Classes</b>";

echo "<table class='tblStyle'>";
echo "<tr class='tblHeaderRow'><td class='tblHeaderCell'> section_id </td><td class='tblHeaderCell'> course_id </td><td class='tblHeaderCell'> title </td><td class='tblHeaderCell'> credits </td><td class='tblHeaderCell'> location </td></tr>";

//display the results
while($row = sqlsrv_fetch_array($result1, SQLSRV_FETCH_ASSOC))
{
  echo "<tr><td class='tblCell'>" . htmlentities($row["section_id"]) . "</td><td class='tblCell'>" . htmlentities($row["course_id"]) . "</td><td class='tblCell'>" . htmlentities($row["title"]) . "</td><td class='tblCell'>" . htmlentities($row["credits"]) . "</td><td class='tblCell'>" . htmlentities($row["location"]) . "</td></tr>";
}
echo "</table class='tblStyle'>";

sqlsrv_close($conn);
}
?>
</body>
</html>
