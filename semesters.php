<!DOCTYPE html>
<html>
<head>
	
	<link rel="stylesheet" type="text/css" href="resources/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="resources/css/custom.css">


	<title>Welcome to ClassReg</title>
</head>
<body>
<nav class="navbar navbar-inverse" role="navigation">
<div class="container-fluid">
	<div class="navbar-header">
	<a class="navbar-brand" href="index.php">ClassReg</a>
	</div>

	<div class="collapse-navbar-collapse">
		<ul class="nav navbar-nav">
			<li><a href="courses.php">Courses</a></li>
			<li class="active"><a href="semesters.php">Semesters</a></li>
		</ul>
	</div>
</div>
</nav>
</body>
</html>