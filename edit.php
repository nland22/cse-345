

<?php

include("database.php");

/* check querystring */

if(isset($_GET["student_id"]))
{
	$student_id = $_GET["student_id"];
}
else
{
	die('There was no primary key parameter passed in, this page can only be accessed from the index.php page which passes a primary key');
}

//declare the SQL statement that will query the database
$query = "SELECT student_id, first_name, last_name FROM dbo.students WHERE student_id = ?";
$params = array($student_id);
$result = sqlsrv_query($conn, $query, $params);

?>

<html>
<head>
<title>PHP Select</title>
<link rel="stylesheet" type="text/css" href="style.css" />
</head>

<body>
<?php

echo "<form action='index.php' method='get'>";
echo "<input type='hidden' name='action' value='update' />";

//Pass existing Querystring params along for paging and sorting
foreach ($_GET as $key => $value) 
{
	if ($key != "C") // ignore this particular $_GET value
	{  
                echo "<input type='hidden' name='" . $key . "' value='" . $value . "' />";
        }
}

//display the results
while($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC))
{
  echo "<label>first_name: </label><input type='text' name='first_name' value='" . $row["first_name"] . "'/>";echo "<br>";echo "<label>last_name: </label><input type='text' name='last_name' value='" . $row["last_name"] . "'/>";
}

echo "<br>";
echo "<input type='submit' value='Update' />";

echo "</form>";


//close the connection
sqlsrv_close($conn);
?>
</body>
</html>