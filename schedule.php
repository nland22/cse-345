<html>
<head>
<?php
include("database.php");
include("utilities.php");

if(isset($_GET["action"]))
{
	$valid = true;
	
	if($_GET["action"] != 'delete') {
		
	}
	
	if ($valid)
	{
		if($_GET["action"] == "update")
		{
			$query = "UPDATE dbo.students SET first_name = ? , last_name = ?  WHERE student_id = ?";
			$params = array(htmlentities($_GET["first_name"],ENT_COMPAT,'iso-8859-1'), htmlentities($_GET["last_name"],ENT_COMPAT,'iso-8859-1'), $_GET["student_id"]);
			sqlsrv_query($conn, $query, $params);
		}

		if($_GET["action"] == "create")
		{
			$query = "INSERT INTO dbo.course_load (student_id, section_id, grade) VALUES (?, ?, ?)";
			$params = array(($_GET["student_id"]),($_GET["section_id"]),"N/A");
			sqlsrv_query($conn, $query, $params);
		}
	}

	if($_GET["action"] == "delete")
	{
	   $query = "DELETE FROM dbo.course_load WHERE section_id = ? AND student_id = ?";
	   $params = array(($_GET["section_id"]),($_GET["student_id"]));
	   sqlsrv_query($conn, $query, $params); 
	   //echo "hello";
	}

}
$query = "SELECT     dbo.course_sections.course_id, dbo.students.student_id, dbo.course_load.section_id
FROM         dbo.course_load INNER JOIN
                      dbo.course_sections ON dbo.course_load.section_id = dbo.course_sections.section_id INNER JOIN
                      dbo.students ON dbo.course_load.student_id = dbo.students.student_id
WHERE     (dbo.students.student_id = ($_GET[student_id]))";
$result = sqlsrv_query($conn, $query);


?>
</head>



<body>

<?php

echo "<form name='form' action='' method='GET'>";
echo "<input type='hidden' name='action' value='create'>";
echo "<label>Section Number: </label><input type='text' name='section_id'/>";
echo "<input type='hidden' name='student_id' value=" . $_GET[student_id] . ">";
echo "<input type='submit'>";
echo "</form>";

$temp=$_GET[section];

echo "<b>Registered Classes</b>";
echo "<table class='tblStyle'>";
echo "<tr class='tblHeaderRow'><td class='tblHeaderCell'> section_id </td><td class='tblHeaderCell'> course_id </td><td class='tblHeaderCell'> Delete </td></tr>";




//display the results
while($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC))
{
  echo "<tr><td class='tblCell'>" . htmlentities($row["section_id"]) . "</td><td class='tblCell'>" . htmlentities($row["course_id"]) . "</td><td class='tblCell'><a href='schedule.php?action=delete&section_id=" . $row["section_id"] . "&student_id=" . $_GET[student_id] . "'>Delete</a></td></tr>";
}
echo "</table style='tblStyle'>";

sqlsrv_close($conn);

?>
</body>
</html>
