<!DOCTYPE html>
<html>
<head>

	<link rel="stylesheet" type="text/css" href="resources/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="resources/css/custom.css">


	<title>Welcome to ClassReg</title>
</head>
<body>
<nav class="navbar navbar-inverse" role="navigation">
<div class="container-fluid">
	<div class="navbar-header">
	<a class="navbar-brand" href="index.php">ClassReg</a>
	</div>

	<div class="collapse-navbar-collapse">
		<ul class="nav navbar-nav">
			<li class="active"><a href="courses.php">Courses</a></li>
			<li><a href="semesters.php">Semesters</a></li>
		</ul>
	</div>
</div>
</nav>
</body>
</html>

<?php

include("database.php");

if($conn) {
	echo "Connection established.</br></br>";
} else {
	echo "Connection could not be established. </br>";
	die(print_r(sqlsrv_errors(), true));
}

$query = "SELECT * FROM dbo.course_sections";
// $params = array(htmlentities($_GET["section_id"],ENT_COMPAT,'iso-8859-1'), htmlentities($_GET["term"],ENT_COMPAT,'iso-8859-1'), $_GET["location"]);
$result = sqlsrv_query($conn, $query);

while($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_BOTH))
{
	echo "<ul><li>".htmlentities($row["section_id"])."</li><li>".htmlentities($row["term"])."</li><li>".htmlentities($row["location"])."</li></ul>";
}
 ?>